#!/usr/bin/env bash
#
# Copyright (c) 2019 ForgeRock AS. All rights reserved.
#

set -x


exit_script() {
    echo "Got signal. Killing child processes"
    trap - SIGINT SIGTERM # clear the trap
    kill -- -$$ # Sends SIGTERM to child/sub processes
    echo "Exiting"
    exit 0
}

trap exit_script SIGINT SIGTERM SIGUSR1 EXIT



pause() {
    echo "Args are $# "

    echo "Container will now pause. You can use kubectl exec to run export.sh"
    # Sleep forever, waiting for someone to exec into the container.
    while true
    do
        sleep 1000000 & wait
    done
}


case $1  in
configure)
    # invoke amster install.
    ls -all
    DIR=`pwd`
    AMSTER_SCRIPTS=${AMSTER_SCRIPTS:-"${DIR}/scripts"}
    if [ -d  ${AMSTER_SCRIPTS} ]; then
        if [ ! -r /var/run/secrets/amster/id_rsa ]; then
            echo "ERROR: Can not find the Amster private key"
            exit 1
        fi

        echo "Executing Amster to configure AM"
        # Need to be in the amster directory, otherwise Amster can't find its libraries.
        cd ${DIR}
        for file in ${AMSTER_SCRIPTS}/*.amster
        do
            echo "Executing Amster script $file"
            sh ./amster ${file}
        done    
    fi
    ;;
*) 
   exec "$@"
esac